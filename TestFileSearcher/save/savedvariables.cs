﻿using Microsoft.Win32;

namespace TestFileSearcher.save
{
    public static class SavedVariables
    {
        public const string KeyName = "Software\\TestFileSearcher";

        /// <summary>
        /// Сохранение параметра в реестр
        /// </summary>
        public static void Save(string key, string body)
        {
            var registryKey = Registry.CurrentUser.CreateSubKey(KeyName, true);
            registryKey.SetValue(key, body);
            registryKey.Close();
        }

        /// <summary>
        /// Чтение параметров
        /// </summary>
        public static string Load(string key)
        {
            var registryKey = Registry.CurrentUser.OpenSubKey(KeyName, false);
            var body = registryKey?.GetValue(key).ToString();
            registryKey?.Close();

            return body;
        }
    }
}