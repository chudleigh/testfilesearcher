﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace TestFileSearcher.search
{
    public sealed class FileSearcher
    {
        /// <summary>
        /// Singleton instance
        /// </summary>
        public static FileSearcher Instance => _instance.Value;

        /// <summary>
        /// Текущий статус поиска
        /// </summary>
        public SearchStatus Status { get; private set; }

        /// <summary>
        /// Время запуска операции поиска
        /// </summary>
        public DateTime? StartDateTime { get; private set; }

        /// <summary>
        /// Время работы поиска
        /// </summary>
        public TimeSpan WorkTimeSpin
        {
            get
            {
                if (StartDateTime.HasValue)
                {
                    if (_continueTime.HasValue) // Поиск останавливался
                    {
                        return DateTime.UtcNow - _continueTime.Value.ToUniversalTime() + _workTimeSpan;
                    }
                    else
                    {
                        return DateTime.UtcNow - StartDateTime.Value.ToUniversalTime();
                    }
                }
                else
                {
                    return new TimeSpan(0);
                }
            }
        }

        /// <summary>
        /// Количество обработтанных файлов
        /// </summary>
        public long FilesCount { get; private set; }

        /// <summary>
        /// Количество проверенных папок
        /// </summary>
        public long DirsCount { get; private set; }

        /// <summary>
        /// Последний проверенный файл, удовлетворяющий маске
        /// </summary>
        public string LastFile { get; private set; }

        /// <summary>
        /// Возникает при завершении операции поиска
        /// </summary>
        public event EventHandler OnSearchCompleted;

        /// <summary>
        /// Возникает при нахождении нужного файла
        /// </summary>
        public event EventHandler<FileInfo> OnFile;

        /// <summary>
        /// Singleton ctor
        /// </summary>
        private FileSearcher()
        {
        }

        /// <summary>
        /// Запуск операции поиска
        /// </summary>
        public void StartSearchAsync(string root, string searchPattern = "*.*", string text = "")
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _pauseTokenSourse.IsPaused = false;

            FilesCount = 0;
            DirsCount = 0;
            _workTimeSpan = new TimeSpan(0);
            _continueTime = null;

            StartDateTime = DateTime.UtcNow;
            Status = SearchStatus.Actived;
            Task.Run(() => DirectorySearch(root, searchPattern, text));
        }

        public void PauseSearch()
        {
            // Сохраним текущее время поиска
            _workTimeSpan = WorkTimeSpin;

            Status = SearchStatus.Paused;
            _pauseTokenSourse.IsPaused = true;
        }

        public void ContinueSearch()
        {
            // Сохраним время продолжения поиска, чтобы потом вычислить общее время работы
            _continueTime = DateTime.UtcNow;

            Status = SearchStatus.Actived;
            _pauseTokenSourse.IsPaused = false;
        }

        public void CancelSearch()
        {
            Status = SearchStatus.Stoped;
            _cancellationTokenSource.Cancel();

            OnSearchCompleted?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Поиск файлов по маске
        /// </summary>
        private async Task DirectorySearch(string root, string searchPattern, string text)
        {
            Stack<string> dirs = new Stack<string>();
            dirs.Push(root);

            while (dirs.Count > 0)
            {
                _ = _pauseTokenSourse.Token.WaitWhilePausedAsync();
                if (_cancellationTokenSource.Token.IsCancellationRequested) return;

                string currentDirPath = dirs.Pop();
                try
                {
                    string[] subDirs = Directory.GetDirectories(currentDirPath);
                    foreach (string subDirPath in subDirs)
                    {
                        DirsCount++;
                        dirs.Push(subDirPath);
                    }
                }
                catch (Exception) // UnauthorizedAccessException, DirectoryNotFoundException, etc
                {
                    continue;
                }

                string[] files = Directory.GetFiles(currentDirPath, searchPattern);

                foreach (string filePath in files)
                {
                    FilesCount++;

                    // Сохраним путь к файлу
                    LastFile = filePath;

                    // Если текст не ввели, то покажем все файлы
                    if (string.IsNullOrEmpty(text) || TextInFile(filePath, text))
                    {
                        // Оповестим, что удалось найти файл, сожержащий строку text
                        OnFile?.Invoke(this, new FileInfo(filePath));
                    }

                    await _pauseTokenSourse.Token.WaitWhilePausedAsync();
                    if (_cancellationTokenSource.Token.IsCancellationRequested) return;
                }
            }

            Status = SearchStatus.Stoped;
            OnSearchCompleted?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Проверка, содержит ли данный файл строку
        /// </summary>
        private bool TextInFile(string filePath, string text)
        {
            try
            {
                string[] lines = File.ReadAllLines(filePath);
                return lines.Any(x => x.Contains(text));
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// singletone
        /// </summary>
        private static readonly Lazy<FileSearcher> _instance = new Lazy<FileSearcher>(() => new FileSearcher());

        /// <summary>
        /// Токенсорс для приостановки операции поиска
        /// </summary>
        private readonly PauseTokenSource _pauseTokenSourse = new PauseTokenSource();

        /// <summary>
        /// Токенсорс для остановки операции поиска
        /// </summary>
        private CancellationTokenSource _cancellationTokenSource;

        /// <summary>
        /// Время возобновления операции поиска
        /// </summary>
        private DateTime? _continueTime;

        /// <summary>
        /// Время работы поиска до остановки
        /// </summary>
        private TimeSpan _workTimeSpan;
    }
}