﻿namespace TestFileSearcher
{
    /// <summary>
    /// Список статусов
    /// </summary>
    public enum SearchStatus
    {
        /// <summary>
        /// Операции поиска остаровлены
        /// </summary>
        Stoped,

        /// <summary>
        /// Поиск активен
        /// </summary>
        Actived,

        /// <summary>
        /// Поиск остановлен
        /// </summary>
        Paused
    }
}