﻿using System.Threading;
using System.Threading.Tasks;

namespace TestFileSearcher.search
{
    public class PauseTokenSource
    {
        private volatile TaskCompletionSource<bool> m_paused;

        public bool IsPaused
        {
            get => m_paused != null;
            set
            {
                if (value)
                {
                    Interlocked.CompareExchange(ref m_paused, new TaskCompletionSource<bool>(), null);
                }
                else
                {
                    while (true)
                    {
                        TaskCompletionSource<bool> tcs = m_paused;
                        if (tcs == null) return;
                        if (Interlocked.CompareExchange(ref m_paused, null, tcs) == tcs)
                        {
                            Task.Run(() => tcs.SetResult(true));
                            break;
                        }
                    }
                }
            }
        }

        public Task WaitWhilePausedAsync()
        {
            TaskCompletionSource<bool> cur = m_paused;
            return cur != null ? cur.Task : s_completedTask;
        }

        public static readonly Task s_completedTask = Task.FromResult(true);

        public PauseToken Token => new PauseToken(this);
    }

    public struct PauseToken
    {
        private readonly PauseTokenSource m_source;

        public PauseToken(PauseTokenSource source)
        {
            m_source = source;
        }

        public bool IsPaused => m_source != null && m_source.IsPaused;

        public Task WaitWhilePausedAsync()
        {
            return IsPaused ? m_source.WaitWhilePausedAsync() : PauseTokenSource.s_completedTask;
        }
    }
}