﻿namespace TestFileSearcher
{
    partial class fmMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fmMain));
            this.timerUpdateInfo = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.te = new System.Windows.Forms.ToolStripStatusLabel();
            this.sslbDirsCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.fs = new System.Windows.Forms.ToolStripStatusLabel();
            this.sslbFilesCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.sslbSearchTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btStartPause = new System.Windows.Forms.Button();
            this.ilButtonsImages = new System.Windows.Forms.ImageList(this.components);
            this.lbCurrentFile = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbSearchPattern = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbFolderPath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btCancel = new System.Windows.Forms.Button();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerUpdateInfo
            // 
            this.timerUpdateInfo.Tick += new System.EventHandler(this.TimerUpdateInfo_Tick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.te,
            this.sslbDirsCount,
            this.toolStripStatusLabel1,
            this.fs,
            this.sslbFilesCount,
            this.toolStripStatusLabel2,
            this.sslbSearchTime,
            this.toolStripStatusLabel3});
            this.statusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.statusStrip1.Location = new System.Drawing.Point(0, 412);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1140, 32);
            this.statusStrip1.TabIndex = 15;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // te
            // 
            this.te.Name = "te";
            this.te.Size = new System.Drawing.Size(219, 25);
            this.te.Text = "Проверено директорий: ";
            // 
            // sslbDirsCount
            // 
            this.sslbDirsCount.AutoSize = false;
            this.sslbDirsCount.Name = "sslbDirsCount";
            this.sslbDirsCount.Size = new System.Drawing.Size(50, 25);
            this.sslbDirsCount.Text = "0";
            this.sslbDirsCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.AutoSize = false;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(10, 25);
            // 
            // fs
            // 
            this.fs.Name = "fs";
            this.fs.Size = new System.Drawing.Size(182, 25);
            this.fs.Text = "Проверено файлов: ";
            // 
            // sslbFilesCount
            // 
            this.sslbFilesCount.AutoSize = false;
            this.sslbFilesCount.Name = "sslbFilesCount";
            this.sslbFilesCount.Size = new System.Drawing.Size(50, 25);
            this.sslbFilesCount.Text = "0";
            this.sslbFilesCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.AutoSize = false;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(10, 25);
            // 
            // sslbSearchTime
            // 
            this.sslbSearchTime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.sslbSearchTime.Name = "sslbSearchTime";
            this.sslbSearchTime.Size = new System.Drawing.Size(80, 25);
            this.sslbSearchTime.Text = "00:00:00";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btStartPause);
            this.panel1.Controls.Add(this.lbCurrentFile);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tbText);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.tbSearchPattern);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.tbFolderPath);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.btCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1140, 154);
            this.panel1.TabIndex = 23;
            // 
            // btStartPause
            // 
            this.btStartPause.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btStartPause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btStartPause.ImageIndex = 1;
            this.btStartPause.ImageList = this.ilButtonsImages;
            this.btStartPause.Location = new System.Drawing.Point(1080, 99);
            this.btStartPause.Name = "btStartPause";
            this.btStartPause.Size = new System.Drawing.Size(40, 40);
            this.btStartPause.TabIndex = 32;
            this.btStartPause.UseVisualStyleBackColor = true;
            this.btStartPause.Click += new System.EventHandler(this.btStartPause_Click);
            // 
            // ilButtonsImages
            // 
            this.ilButtonsImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilButtonsImages.ImageStream")));
            this.ilButtonsImages.TransparentColor = System.Drawing.Color.Transparent;
            this.ilButtonsImages.Images.SetKeyName(0, "iconfinder_24_Stop_106221.png");
            this.ilButtonsImages.Images.SetKeyName(1, "iconfinder_11_Search_106236.png");
            this.ilButtonsImages.Images.SetKeyName(2, "iconfinder_pause-circle-outline_326570.png");
            // 
            // lbCurrentFile
            // 
            this.lbCurrentFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCurrentFile.AutoEllipsis = true;
            this.lbCurrentFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCurrentFile.Location = new System.Drawing.Point(166, 104);
            this.lbCurrentFile.Name = "lbCurrentFile";
            this.lbCurrentFile.Size = new System.Drawing.Size(857, 26);
            this.lbCurrentFile.TabIndex = 31;
            this.lbCurrentFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(19, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 26);
            this.label2.TabIndex = 30;
            this.label2.Text = "Файл:  ";
            // 
            // tbText
            // 
            this.tbText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbText.Location = new System.Drawing.Point(474, 56);
            this.tbText.Name = "tbText";
            this.tbText.Size = new System.Drawing.Size(646, 32);
            this.tbText.TabIndex = 29;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(388, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 26);
            this.label4.TabIndex = 28;
            this.label4.Text = "Текст: ";
            // 
            // tbSearchPattern
            // 
            this.tbSearchPattern.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbSearchPattern.Location = new System.Drawing.Point(171, 56);
            this.tbSearchPattern.Name = "tbSearchPattern";
            this.tbSearchPattern.Size = new System.Drawing.Size(211, 32);
            this.tbSearchPattern.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(19, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 26);
            this.label1.TabIndex = 26;
            this.label1.Text = "Маска: ";
            // 
            // tbFolderPath
            // 
            this.tbFolderPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFolderPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbFolderPath.Location = new System.Drawing.Point(171, 14);
            this.tbFolderPath.Name = "tbFolderPath";
            this.tbFolderPath.Size = new System.Drawing.Size(949, 32);
            this.tbFolderPath.TabIndex = 25;
            this.tbFolderPath.Resize += new System.EventHandler(this.tbFolderPath_Resize);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(19, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(146, 26);
            this.label3.TabIndex = 24;
            this.label3.Text = "Директория: ";
            // 
            // btCancel
            // 
            this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btCancel.ImageIndex = 0;
            this.btCancel.ImageList = this.ilButtonsImages;
            this.btCancel.Location = new System.Drawing.Point(1034, 99);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(40, 40);
            this.btCancel.TabIndex = 23;
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Visible = false;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Location = new System.Drawing.Point(0, 154);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(1140, 258);
            this.treeView1.TabIndex = 24;
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(188, 25);
            this.toolStripStatusLabel3.Text = "Продолжительность: ";
            this.toolStripStatusLabel3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            // 
            // fmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1140, 444);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.MinimumSize = new System.Drawing.Size(900, 500);
            this.Name = "fmMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.fmMain_FormClosing);
            this.Shown += new System.EventHandler(this.fmMain_Shown);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timerUpdateInfo;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel te;
        private System.Windows.Forms.ToolStripStatusLabel sslbFilesCount;
        private System.Windows.Forms.ToolStripStatusLabel fs;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel sslbSearchTime;
        private System.Windows.Forms.ToolStripStatusLabel sslbDirsCount;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tbText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbSearchPattern;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbFolderPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Label lbCurrentFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btStartPause;
        private System.Windows.Forms.ImageList ilButtonsImages;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
    }
}

