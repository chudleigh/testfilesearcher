﻿using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using TestFileSearcher.save;
using TestFileSearcher.search;

namespace TestFileSearcher
{
    public partial class fmMain : Form
    {
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);

        private const int EM_SETMARGINS = 0xd3;
        private const int EC_RIGHTMARGIN = 0x2;

        public fmMain()
        {
            InitializeComponent();

            FileSearcher.Instance.OnSearchCompleted += Searcher_OnSearchCompleted;
            FileSearcher.Instance.OnFile += Instance_OnFile;
        }

        protected override void OnLoad(EventArgs e)
        {
            Button btBrowse = new Button
            {
                Size = new Size(tbFolderPath.ClientSize.Height + 10, tbFolderPath.ClientSize.Height + 2)
            };
            btBrowse.TextAlign = ContentAlignment.TopCenter;
            btBrowse.Location = new Point(tbFolderPath.ClientSize.Width - btBrowse.Width, -1);
            btBrowse.Text = "···";
            btBrowse.Cursor = DefaultCursor;
            btBrowse.Click += btBrowse_Click;
            tbFolderPath.Controls.Add(btBrowse);

            SendMessage(tbFolderPath.Handle, EM_SETMARGINS, (IntPtr)EC_RIGHTMARGIN, (IntPtr)(btBrowse.Width << 16));
            base.OnLoad(e);
        }

        /// <summary>
        /// Возникает при первом отображении формы
        /// </summary>
        private void fmMain_Shown(object sender, EventArgs e)
        {
            LoadSavedVariables();
        }

        /// <summary>
        /// Возникает при закрытии формы
        /// </summary>
        private void fmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveVariables();
        }

        /// <summary>
        /// Возникает при изменении размеров поля "Путь"
        /// </summary>
        private void tbFolderPath_Resize(object sender, EventArgs e)
        {
            if (tbFolderPath.Controls.Count == 1)
            {
                Control btBrowse = tbFolderPath.Controls[0];
                btBrowse.Location = new Point(tbFolderPath.ClientSize.Width - btBrowse.Width, -1);
            }
        }

        /// <summary>
        /// Выбор директории
        /// </summary>
        private void btBrowse_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                tbFolderPath.Text = folderBrowserDialog.SelectedPath;
            }
        }

        /// <summary>
        /// Запуск \ Пауза \ Возобновление поиска
        /// </summary>
        private void btStartPause_Click(object sender, EventArgs e)
        {
            switch (FileSearcher.Instance.Status)
            {
                case SearchStatus.Stoped:
                    StartSearch();
                    break;

                case SearchStatus.Paused:
                    ContinueSearch();
                    break;

                case SearchStatus.Actived:
                    PauseSearch();
                    break;
            }
        }

        /// <summary>
        /// Остановка операции поиска
        /// </summary>
        private void btCancel_Click(object sender, EventArgs e)
        {
            CancelSearch();
        }

        /// <summary>
        /// Возникает при остановке или завершении операции поиска
        /// </summary>
        private void Searcher_OnSearchCompleted(object sender, EventArgs e)
        {
            Invoke(new Action(() =>
            {
                // Если операция поиска завершилась быстрее тика таймера, то форма будет пуста
                TimerUpdateInfo_Tick(timerUpdateInfo, e); // Покажем последние данные

                btStartPause.ImageIndex = 1;
                btCancel.Visible = false;
                lbCurrentFile.Text = string.Empty;
                timerUpdateInfo.Enabled = false;
                tbFolderPath.Enabled = true;
                tbSearchPattern.Enabled = true;
                tbText.Enabled = true;
            }));
        }

        /// <summary>
        /// Возникает при нахождении подходящего файла
        /// </summary>
        private void Instance_OnFile(object sender, FileInfo e)
        {
            Invoke(new Action(() =>
            {
                AddFile(e);
                treeView1.ExpandAll();
            }));
        }

        /// <summary>
        /// Тик таймера для обновления информации на форме
        /// </summary>
        private void TimerUpdateInfo_Tick(object sender, EventArgs e)
        {
            sslbDirsCount.Text = FileSearcher.Instance.DirsCount.ToString();
            sslbFilesCount.Text = FileSearcher.Instance.FilesCount.ToString();
            sslbSearchTime.Text = FileSearcher.Instance.WorkTimeSpin.ToString("hh\\:mm\\:ss");
            lbCurrentFile.Text = FileSearcher.Instance.LastFile;
        }

        /// <summary>
        /// Запуск операции поиска
        /// </summary>
        private void StartSearch()
        {
            int length = tbFolderPath.Text.Length * tbSearchPattern.Text.Length * tbText.Text.Length;

            if (length != 0)
            {
                btStartPause.ImageIndex = 2;
                btCancel.Visible = true;
                timerUpdateInfo.Enabled = true;
                tbFolderPath.Enabled = false;
                tbSearchPattern.Enabled = false;
                tbText.Enabled = false;
                treeView1.Nodes.Clear();
                FileSearcher.Instance.StartSearchAsync(tbFolderPath.Text, tbSearchPattern.Text, tbText.Text);
            }
            else
            {
                MessageBox.Show("Необходимо заполнить все поля", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Продолжение операции поиска
        /// </summary>
        private void ContinueSearch()
        {
            btStartPause.ImageIndex = 2;
            timerUpdateInfo.Enabled = true;
            FileSearcher.Instance.ContinueSearch();
        }

        /// <summary>
        /// Приостановка операции поиска
        /// </summary>
        private void PauseSearch()
        {
            btStartPause.ImageIndex = 1;
            timerUpdateInfo.Enabled = false;
            FileSearcher.Instance.PauseSearch();
        }

        /// <summary>
        /// Остановка операции поиска
        /// </summary>
        private void CancelSearch()
        {
            btStartPause.ImageIndex = 1;
            btCancel.Visible = false;
            timerUpdateInfo.Enabled = false;
            FileSearcher.Instance.CancelSearch();
            lbCurrentFile.Text = string.Empty;
            tbFolderPath.Enabled = true;
            tbSearchPattern.Enabled = true;
            tbText.Enabled = true;
        }

        /// <summary>
        /// Рекурсивное добавление узлов в TreeView
        /// </summary>
        private TreeNode AddDirectory(DirectoryInfo dir)
        {
            TreeNodeCollection level = dir.Parent == null ? treeView1.Nodes : AddDirectory(dir.Parent).Nodes;
            return level[dir.Name] ?? level.Add(dir.Name, dir.Name);
        }

        /// <summary>
        /// Добавление узлов в treeView для файла
        /// </summary>
        private TreeNode AddFile(FileInfo file)
        {
            TreeNode dir = AddDirectory(file.Directory);
            return dir.Nodes.Add(file.Name);
        }

        /// <summary>
        /// Сохранение данных из текстовых полей
        /// </summary>
        private void SaveVariables()
        {
            try
            {
                SavedVariables.Save("Directory", tbFolderPath.Text);
                SavedVariables.Save("Pattern", tbSearchPattern.Text);
                SavedVariables.Save("Text", tbText.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка сохранения данных:{ex}", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Загрузка сохранённых данных
        /// </summary>
        private void LoadSavedVariables()
        {
            try
            {
                tbFolderPath.Text = SavedVariables.Load("Directory");
                tbSearchPattern.Text = SavedVariables.Load("Pattern");
                tbText.Text = SavedVariables.Load("Text");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка загрузки данных:{ex}", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}